<?php

class Mahasiswa_model extends CI_Model
{
    protected $_table = "mahasiswa";

    public function getAll($id = null)
    {
        // $this->db->select([
        //     "m.id",
        //     "m.nim",
        //     "m.nama",
        //     "m.jurusan",
        //     "m.universitas",
        //     "p.posisi as posisi_magang"
        // ]);
        // $this->db->join("posisi p", "p.id_posisi = m.posisi_magang", "left");

        if ($id === null) {
            
            return $this->db->get($this->_table )->result_array();
        } else if($id){
            return $this->db->get_where($this->_table, ['id' => $id])->result_array();
        } 
    }
    public function getNim($nim)
    {
        return $this->db->get_where($this->_table, ['nim' => $nim])->result_array();
    }
    public function getNama($nama)
    {
        return $this->db->get_where($this->_table, ['nama' => $nama])->result_array();
    }
    public function getJurusan($jurusan)
    {
        return $this->db->get_where($this->_table, ['jurusan' => $jurusan])->result_array();
    }
    public function getUniv($universitas)
    {
        return $this->db->get_where($this->_table, ['universitas' => $universitas])->result_array();
    }
    public function getPosisi($posisi_magang)
    {
        return $this->db->get_where($this->_table, ['posisi_magang' => $posisi_magang])->result_array();
    }

    public function orderBy($orderBy = null)
    {
        if($orderBy){
            if($orderBy == "nim asc"){
                $this->db->order_by("nim asc");
                return $this->db->get($this->_table)->result_array();
            } 
            else if($orderBy == "nama asc"){
                $this->db->order_by("nama asc");
                return $this->db->get($this->_table)->result_array();
            }
            else if($orderBy == "jurusan asc"){
                $this->db->order_by("jurusan asc");
                return $this->db->get($this->_table)->result_array();
            }
            else if($orderBy == "universitas asc"){
                $this->db->order_by("universitas asc");
                return $this->db->get($this->_table)->result_array();
            }
            else if($orderBy == "posisi asc"){
                $this->db->order_by("posisi_magang asc");
                return $this->db->get($this->_table)->result_array();
            } else{
                if($orderBy == "nim desc"){
                    $this->db->order_by("nim desc");
                    return $this->db->get($this->_table)->result_array();
                }
                else if($orderBy == "nama desc"){
                    $this->db->order_by("nama desc");
                    return $this->db->get($this->_table)->result_array();
                }
                else if($orderBy == "jurusan desc"){
                    $this->db->order_by("jurusan desc");
                    return $this->db->get($this->_table)->result_array();
                }
                else if($orderBy == "universitas desc"){
                    $this->db->order_by("universitas desc");
                    return $this->db->get($this->_table)->result_array();
                }
                else if($orderBy == "posisi desc"){
                    $this->db->order_by("posisi_magang desc");
                    return $this->db->get($this->_table)->result_array();
                }
            }

        }
    }

    public function createMhs($data)
    {
        $this->db->insert($this->_table, $data);
        return $this->db->affected_rows();
    }

    public function updateMhs($data, $id)
    {
        $this->db->update($this->_table, $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function deleteMhs($id = null)
    {
        $this->db->delete($this->_table, ['id' => $id]);
        return $this->db->affected_rows();
    }
}
