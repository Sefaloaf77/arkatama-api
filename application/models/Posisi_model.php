<?php

class Posisi_model extends CI_Model
{
    protected $_table = "posisi";

    public function getAll($id = null)
    {
        if ($id === null) {
            return $this->db->get($this->_table)->result_array();
        } else if ($id) {
            return $this->db->get_where($this->_table, ['id_posisi' => $id])->result_array();
        }
    }
    public function getPosisi($posisi)
    {
        return $this->db->get_where($this->_table, ['posisi' => $posisi])->result_array();
    }

    public function orderBy($orderBy = null)
    {
        if ($orderBy) {
            if ($orderBy == "posisi asc") {
                $this->db->order_by("posisi asc");
                return $this->db->get($this->_table)->result_array();
            } else if ($orderBy == "posisi desc") {
                $this->db->order_by("posisi desc");
                return $this->db->get($this->_table)->result_array();
            }
        }
    }

    public function createPosisi($data)
    {
        $this->db->insert($this->_table, $data);
        return $this->db->affected_rows();
    }

    public function updatePosisi($data, $id)
    {
        $this->db->update($this->_table, $data, ['id_posisi' => $id]);
        return $this->db->affected_rows();
    }

    public function deletePosisi($id = null)
    {
        $this->db->delete($this->_table, ['id' => $id]);
        return $this->db->affected_rows();
    }
}
