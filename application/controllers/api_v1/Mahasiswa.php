<?php
defined('BASEPATH') or exit('No direct script access allowed');


use chriskacerguis\RestServer\RestController;

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';


class Mahasiswa extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mahasiswa_model', 'Mmhs');
    }

    public function index_get(){
        $id = $this->get('id');
        $nama = $this->get('nama');
        $nim = $this->get('nim');
        $jurusan = $this->get('jurusan');
        $universitas = $this->get('universitas');
        $posisi_magang = $this->get('posisi_magang');
        $orderBy = $this->get('orderBy');
        
        if($id){
            $mahasiswa = $this->Mmhs->getAll($id);
            if ($mahasiswa) {
                $this->response([
                    'status' => 'success',
                    'message' => 'Data Founded',
                    'data' => $this->Mmhs->getAll($id)
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found',
                    'data' => $this->Mmhs->getAll($id),
                    'errorCode' => RestController::HTTP_NOT_FOUND
                ], RestController::HTTP_NOT_FOUND);
            }
        }
        else if($nama){
            $mahasiswa = $this->Mmhs->getNama($nama);
            if ($mahasiswa) {
                $this->response([
                    'status' => 'success',
                    'message' => 'Data Founded',
                    'data' => $this->Mmhs->getNama($nama)
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found',
                    'data' => $this->Mmhs->getNama($nama),
                    'errorCode' => RestController::HTTP_NOT_FOUND
                ], RestController::HTTP_NOT_FOUND);
            }
        }
        else if($nim){
            $mahasiswa = $this->Mmhs->getNim($nim);
            if ($mahasiswa) {
                $this->response([
                    'status' => 'success',
                    'message' => 'Data Founded',
                    'data' => $this->Mmhs->getNim($nim)
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found',
                    'data' => $this->Mmhs->getNim($nim),
                    'errorCode' => RestController::HTTP_NOT_FOUND
                ], RestController::HTTP_NOT_FOUND);
            }
        }
        else if($jurusan){
            $mahasiswa = $this->Mmhs->getJurusan($jurusan);
            if ($mahasiswa) {
                $this->response([
                    'status' => 'success',
                    'message' => 'Data Founded',
                    'data' => $this->Mmhs->getJurusan($jurusan)
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found',
                    'data' => $this->Mmhs->getJurusan($jurusan),
                    'errorCode' => RestController::HTTP_NOT_FOUND
                ], RestController::HTTP_NOT_FOUND);
            }
        }
        else if($universitas){
            $mahasiswa = $this->Mmhs->getUniv($universitas);
            if ($mahasiswa) {
                $this->response([
                    'status' => 'success',
                    'message' => 'Data Founded',
                    'data' => $this->Mmhs->getUniv($universitas)
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found',
                    'data' => $this->Mmhs->getUniv($universitas),
                    'errorCode' => RestController::HTTP_NOT_FOUND
                ], RestController::HTTP_NOT_FOUND);
            }
        }
        else if($posisi_magang){
            $mahasiswa = $this->Mmhs->getPosisi($posisi_magang);
            if ($mahasiswa) {
                $this->response([
                    'status' => 'success',
                    'message' => 'Data Founded',
                    'data' => $this->Mmhs->getPosisi($posisi_magang)
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found',
                    'data' => $this->Mmhs->getPosisi($posisi_magang),
                    'errorCode' => RestController::HTTP_NOT_FOUND
                ], RestController::HTTP_NOT_FOUND);
            }
        } 
        
        else if ($orderBy){
            if($orderBy == "nim asc"){
                $mahasiswa = $this->Mmhs->orderBy($orderBy);
                if ($mahasiswa) {
                    $this->response([
                        'status' => 'success',
                        'message' => 'Data has been ordered',
                        'data' => $this->Mmhs->orderBy($orderBy)
                    ], RestController::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 'failed',
                        'message' => 'id not found',
                        'data' => $this->Mmhs->orderBy($orderBy),
                        'errorCode' => RestController::HTTP_NOT_FOUND
                    ], RestController::HTTP_NOT_FOUND);
                }
            } 
            else if ($orderBy == "nama asc") {
                $mahasiswa = $this->Mmhs->orderBy($orderBy);
                if ($mahasiswa) {
                    $this->response([
                        'status' => 'success',
                        'message' => 'Data has been ordered',
                        'data' => $this->Mmhs->orderBy($orderBy)
                    ], RestController::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 'failed',
                        'message' => 'id not found',
                        'data' => $this->Mmhs->orderBy($orderBy),
                        'errorCode' => RestController::HTTP_NOT_FOUND
                    ], RestController::HTTP_NOT_FOUND);
                }
            }
            else if ($orderBy == "jurusan asc") {
                $mahasiswa = $this->Mmhs->orderBy($orderBy);
                if ($mahasiswa) {
                    $this->response([
                        'status' => 'success',
                        'message' => 'Data has been ordered',
                        'data' => $this->Mmhs->orderBy($orderBy)
                    ], RestController::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 'failed',
                        'message' => 'id not found',
                        'data' => $this->Mmhs->orderBy($orderBy),
                        'errorCode' => RestController::HTTP_NOT_FOUND
                    ], RestController::HTTP_NOT_FOUND);
                }
            }
            else if ($orderBy == "universitas asc") {
                $mahasiswa = $this->Mmhs->orderBy($orderBy);
                if ($mahasiswa) {
                    $this->response([
                        'status' => 'success',
                        'message' => 'Data has been ordered',
                        'data' => $this->Mmhs->orderBy($orderBy)
                    ], RestController::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 'failed',
                        'message' => 'id not found',
                        'data' => $this->Mmhs->orderBy($orderBy),
                        'errorCode' => RestController::HTTP_NOT_FOUND
                    ], RestController::HTTP_NOT_FOUND);
                }
            }
            else if ($orderBy == "posisi asc") {
                $mahasiswa = $this->Mmhs->orderBy($orderBy);
                if ($mahasiswa) {
                    $this->response([
                        'status' => 'success',
                        'message' => 'Data has been ordered',
                        'data' => $this->Mmhs->orderBy($orderBy)
                    ], RestController::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 'failed',
                        'message' => 'id not found',
                        'data' => $this->Mmhs->orderBy($orderBy),
                        'errorCode' => RestController::HTTP_NOT_FOUND
                    ], RestController::HTTP_NOT_FOUND);
                }
            } else{
                if($orderBy == "nim desc"){
                    $mahasiswa = $this->Mmhs->orderBy($orderBy);
                    if ($mahasiswa) {
                        $this->response([
                            'status' => 'success',
                            'message' => 'Data has been ordered',
                            'data' => $this->Mmhs->orderBy($orderBy)
                        ], RestController::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => 'failed',
                            'message' => 'id not found',
                            'data' => $this->Mmhs->orderBy($orderBy),
                            'errorCode' => RestController::HTTP_NOT_FOUND
                        ], RestController::HTTP_NOT_FOUND);
                    }
                } 
                else if ($orderBy == "nama desc") {
                    $mahasiswa = $this->Mmhs->orderBy($orderBy);
                    if ($mahasiswa) {
                        $this->response([
                            'status' => 'success',
                            'message' => 'Data has been ordered',
                            'data' => $this->Mmhs->orderBy($orderBy)
                        ], RestController::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => 'failed',
                            'message' => 'id not found',
                            'data' => $this->Mmhs->orderBy($orderBy),
                            'errorCode' => RestController::HTTP_NOT_FOUND
                        ], RestController::HTTP_NOT_FOUND);
                    }
                }
                else if ($orderBy == "jurusan desc") {
                    $mahasiswa = $this->Mmhs->orderBy($orderBy);
                    if ($mahasiswa) {
                        $this->response([
                            'status' => 'success',
                            'message' => 'Data has been ordered',
                            'data' => $this->Mmhs->orderBy($orderBy)
                        ], RestController::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => 'failed',
                            'message' => 'id not found',
                            'data' => $this->Mmhs->orderBy($orderBy),
                            'errorCode' => RestController::HTTP_NOT_FOUND
                        ], RestController::HTTP_NOT_FOUND);
                    }
                }
                else if ($orderBy == "universitas desc") {
                    $mahasiswa = $this->Mmhs->orderBy($orderBy);
                    if ($mahasiswa) {
                        $this->response([
                            'status' => 'success',
                            'message' => 'Data has been ordered',
                            'data' => $this->Mmhs->orderBy($orderBy)
                        ], RestController::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => 'failed',
                            'message' => 'id not found',
                            'data' => $this->Mmhs->orderBy($orderBy),
                            'errorCode' => RestController::HTTP_NOT_FOUND
                        ], RestController::HTTP_NOT_FOUND);
                    }
                }
                else if ($orderBy == "posisi desc") {
                    $mahasiswa = $this->Mmhs->orderBy($orderBy);
                    if ($mahasiswa) {
                        $this->response([
                            'status' => 'success',
                            'message' => 'Data has been ordered',
                            'data' => $this->Mmhs->orderBy($orderBy)
                        ], RestController::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => 'failed',
                            'message' => 'id not found',
                            'data' => $this->Mmhs->orderBy($orderBy),
                            'errorCode' => RestController::HTTP_NOT_FOUND
                        ], RestController::HTTP_NOT_FOUND);
                    }
                }
            }
        } else if ($id === null) {
            $mahasiswa = $this->Mmhs->getAll();
            if ($mahasiswa) {
                $this->response([
                    'status' => 'success',
                    'message' => 'Data Founded',
                    'data' => $this->Mmhs->getAll()
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found',
                    'data' => $this->Mmhs->getAll(),
                    'errorCode' => RestController::HTTP_NOT_FOUND
                ], RestController::HTTP_NOT_FOUND);
            }
        } 
        
    }

    public function index_post()
    {
        $data = [
            'nim' => $this->post('nim'),
            'nama' => $this->post('nama'),
            'jurusan' => $this->post('jurusan'),
            'universitas' => $this->post('universitas'),
            'posisi_magang' => $this->post('posisi_magang'),

        ];

        if($this->Mmhs->createMhs($data) > 0){
            $this->response([
                'status' => 'success',
                'message' => 'New mahasiswa has been created',
                'data' => $this->Mmhs->getAll()
            ], RestController::HTTP_CREATED);
        } else{
            $this->response([
                'status' => 'failed',
                'message' => 'failed to create new mahasiswa'
            ], RestController::HTTP_BAD_REQUEST);
        }
    }
    public function index_put()
    {
        $id = $this->put('id');
        $data = [
            'nim' => $this->put('nim'),
            'nama' => $this->put('nama'),
            'jurusan' => $this->put('jurusan'),
            'universitas' => $this->post('universitas'),
            'posisi_magang' => $this->post('posisi_magang')
        ];

        if ($this->Mmhs->updateMhs($data, $id) > 0) {
            $this->response([
                'status' => 'success',
                'message' => 'data has been updated',
                'data' => $this->Mmhs->getAll()
            ], RestController::HTTP_OK);
        } else {
            $this->response([
                'status' => 'failed',
                'message' => 'failed to update data'
            ], RestController::HTTP_BAD_REQUEST);
        }
    }
    public function index_delete()
    {
        $id = $this->delete('id');

        if ($id === null) {
            $this->response([
                'status' => 'failed',
                'message' => 'provide an id!'
            ], RestController::HTTP_BAD_REQUEST);
        } else{
            if($this->Mmhs->deleteMhs($id)>0){
                $this->response([
                    'status' => 'success',
                    'id' => $id,
                    'message' => 'Data deleted'
                ], RestController::HTTP_OK);
            } else{
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found'
                ], RestController::HTTP_BAD_REQUEST);
            }
        }


    }
}
