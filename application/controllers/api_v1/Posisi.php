<?php
defined('BASEPATH') or exit('No direct script access allowed');


use chriskacerguis\RestServer\RestController;

require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';


class Posisi extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Posisi_model', 'Mposisi');
    }

    public function index_get()
    {
        $id = $this->get('id_posisi');
        $posisi = $this->get('posisi');
        $orderBy = $this->get('orderBy');

        if ($id) {
            $posMagang = $this->Mposisi->getAll($id);
            if ($posMagang) {
                $this->response([
                    'status' => 'success',
                    'message' => 'Data Founded',
                    'data' => $this->Mposisi->getAll($id)
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found',
                    'data' => $this->Mposisi->getAll($id),
                    'errorCode' => RestController::HTTP_NOT_FOUND
                ], RestController::HTTP_NOT_FOUND);
            }
        } else if($posisi){
            $posMagang = $this->Mposisi->getPosisi($posisi);
            if ($posMagang) {
                $this->response([
                    'status' => 'success',
                    'message' => 'Data Founded',
                    'data' => $this->Mposisi->getPosisi($posisi)
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found',
                    'data' => $this->Mposisi->getPosisi($posisi),
                    'errorCode' => RestController::HTTP_NOT_FOUND
                ], RestController::HTTP_NOT_FOUND);
            }
        }
         else if ($orderBy != null) {
            if ($orderBy == "posisi asc") {
                $posMagang = $this->Mposisi->orderBy($orderBy);
                if ($posMagang) {
                    $this->response([
                        'status' => 'success',
                        'message' => 'Data has been ordered',
                        'data' => $posMagang
                    ], RestController::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 'failed',
                        'message' => 'id not found',
                        'data' => $posMagang,
                        'errorCode' => RestController::HTTP_NOT_FOUND
                    ], RestController::HTTP_NOT_FOUND);
                }
            } else if ($orderBy == "posisi desc") {
                $posMagang = $this->Mposisi->orderBy($orderBy);
                if ($posMagang) {
                    $this->response([
                        'status' => 'success',
                        'message' => 'Data has been ordered',
                        'data' => $posMagang
                    ], RestController::HTTP_OK);
                } else {
                    $this->response([
                        'status' => 'failed',
                        'message' => 'id not found',
                        'data' => $posMagang,
                        'errorCode' => RestController::HTTP_NOT_FOUND
                    ], RestController::HTTP_NOT_FOUND);
                }
            }
        }
        else if ($id === null) {
            $posMagang = $this->Mposisi->getAll();
            if ($posMagang) {
                $this->response([
                    'status' => 'success',
                    'message' => 'Data Founded',
                    'data' => $posMagang
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found',
                    'data' => $posMagang
                ], RestController::HTTP_NOT_FOUND);
            }
        } 
        

    }

    public function index_post()
    {
        $data = [
            'posisi' => $this->post('posisi')
        ];

        if ($this->Mposisi->createPosisi($data) > 0) {
            $this->response([
                'status' => 'success',
                'message' => 'New posisi has been created',
                'data' => $this->Mposisi->getAll()
            ], RestController::HTTP_CREATED);
        } else {
            $this->response([
                'status' => 'failed',
                'message' => 'failed to create new posisi'
            ], RestController::HTTP_BAD_REQUEST);
        }
    }
    public function index_put()
    {
        $id = $this->put('id');
        $data = [
            'posisi' => $this->put('posisi')
        ];

        if ($this->Mposisi->updatePosisi($data, $id) > 0) {
            $this->response([
                'status' => 'success',
                'message' => 'data has been updated',
                'data' => $this->Mposisi->getAll()
            ], RestController::HTTP_OK);
        } else {
            $this->response([
                'status' => 'failed',
                'message' => 'failed to update data'
            ], RestController::HTTP_BAD_REQUEST);
        }
    }
    public function index_delete()
    {
        $id = $this->delete('id');

        if ($id === null) {
            $this->response([
                'status' => 'failed',
                'message' => 'provide an id!'
            ], RestController::HTTP_BAD_REQUEST);
        } else {
            if ($this->Mposisi->deletePosisi($id) > 0) {
                $this->response([
                    'status' => 'success',
                    'id' => $id,
                    'message' => 'Data deleted'
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    'status' => 'failed',
                    'message' => 'id not found'
                ], RestController::HTTP_BAD_REQUEST);
            }
        }
    }
}
